from kafka import KafkaConsumer
import json
consumer = KafkaConsumer(bootstrap_servers='localhost:9092', value_deserializer=lambda m: json.loads(m.decode('utf-8')))
consumer.subscribe(['adaptation_engine'])
for msg in consumer:
    print (msg)