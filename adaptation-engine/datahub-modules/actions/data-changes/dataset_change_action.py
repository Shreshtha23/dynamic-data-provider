from datahub_actions.action.action import Action
from datahub_actions.event.event_envelope import EventEnvelope
from datahub_actions.pipeline.pipeline_context import PipelineContext
from kafka import KafkaProducer
import json


class DatasetChangedAction(Action):
    @classmethod
    def create(cls, config_dict: dict, ctx: PipelineContext) -> "Action":
        # Simply print the config_dict.
        print(config_dict)
        global producer 
        global topic
        producer = KafkaProducer(bootstrap_servers=config_dict["kafka_bootstrap"], value_serializer=lambda v: json.dumps(v).encode('utf-8'))
        topic = config_dict["kafka_topic"]
        return cls(ctx)

    def __init__(self, ctx: PipelineContext):
        self.ctx = ctx

    def act(self, event: EventEnvelope) -> None:
        print('####' + event.event.__dict__)
        #event example: EventEnvelope(event_type='EntityChangeEvent_v1', event=EntityChangeEvent({'entityType': 'dataset', 'entityUrn': 'urn:li:dataset:(urn:li:dataPlatform:s3,curated/lsp01/uc1/dataset/synthetic_data/preprocessed/LCGRM1_1_11_15_54/LCGRM1_1_11_15_54.csv,PROD)', 'category': 'LIFECYCLE', 'operation': 'CREATE', 'modifier': None, 'parameters': None, 'auditStamp': AuditStampClass({'time': 1669215600649, 'actor': 'urn:li:corpuser:__datahub_system', 'impersonator': None, 'message': None}), 'version': 0}), meta={'kafka': {'topic': 'PlatformEvent_v1', 'offset': 641, 'partition': 0}})
        #event example: EventEnvelope(event_type='EntityChangeEvent_v1', event=EntityChangeEvent({'entityType': 'dataset', 'entityUrn': 'urn:li:dataset:(urn:li:dataPlatform:s3,curated/lsp01/uc1/dataset/synthetic_data/preprocessed/LCGRM1_1_11_15_54/LCGRM1_1_11_15_54.csv,PROD)', 'category': 'LIFECYCLE', 'operation': 'HARD_DELETE', 'modifier': None, 'parameters': None, 'auditStamp': AuditStampClass({'time': 1669215384199, 'actor': 'urn:li:corpuser:__datahub_system', 'impersonator': None, 'message': None}), 'version': 0}), meta={'kafka': {'topic': 'PlatformEvent_v1', 'offset': 629, 'partition': 0}})
        
        producer.send(topic, {"dataObjectID": "test1"})
        #producer.send(topic, event.event.__dict__)
    def close(self) -> None:
        pass
