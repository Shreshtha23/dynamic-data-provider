
import boto
from boto.s3.connection import OrdinaryCallingFormat

rc_access_key = 'admin'
rc_secret_key = 'password'
#rc_access_key = os.environ['ACCESS_KEY']
#rc_secret_key = os.environ['SECRET_KEY']
presigned_url_validity = 60


c = boto.connect_s3(
    aws_access_key_id=rc_access_key,
    aws_secret_access_key=rc_secret_key,
    host='bd4nrg-htap.vm.fedcloud.eu',
    port=9005,
    is_secure=False,  # uncomment if you are not using ssl
    calling_format=OrdinaryCallingFormat(),
)

response = c.get_all_buckets()

# Output the bucket names
print('Existing buckets:')
for b in response:
    print(b.name)