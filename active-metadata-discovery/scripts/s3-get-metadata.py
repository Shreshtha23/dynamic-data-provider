import boto3
s3client = boto3.client(
    's3',
    aws_access_key_id= 'admin',
    aws_secret_access_key='password',
    endpoint_url= 'http://bd4nrg-htap.vm.fedcloud.eu:9005'
)

paginator = s3client.get_paginator('list_objects_v2')
page_iterator = paginator.paginate(Bucket='curated')
for bucket in page_iterator:
    for file in bucket['Contents']:
        print(file['Key'])
        try:
            metadata = s3client.head_object(Bucket='curated', Key=file['Key'])
            #print(metadata)
            for key in {'x-amz-meta-event_code',
                        'x-amz-meta-first_timestamp',
                        'x-amz-meta-panel_code',
                        'x-amz-meta-substation_code',
                        'x-amz-meta-trigger_timestamp'}:
              tag = key.split('-')[-1]+":"+metadata['ResponseMetadata']['HTTPHeaders'][key]
              print(tag)
        except:
            print("Failed {}".format(file['Key']))