S3_URL='s3://trusted/lsp01/uc1/dataset/202210/'
FILES=('2019_12032_1_1_SP2_20191120_15_15_06_173_modified.csv' '2019_5984_2_2_SP2_20190713_13_03_55_modified.csv' '2020_1977_4_4_SP2_20200221_03_56_30_modified.csv' '2020_3699_5_5_SP1_20200331_09_18_04_modified.csv' '2020_4028_8_8_SP2_20200409_22_14_46_modified.csv' '2020_4431_9_9_SP2_20200423_08_23_43_modified.csv' '2020_7006_11_11_SP2_20200708_20_08_47_modified.csv' '202210/2021_5236_7_9_SP1_20210420_21_52_14_modified.csv')
EVENT_CODES=()
for file in "${FILES[@]}"
do
  echo "- include: '$S3_URL$file'"
done
for file in "${FILES[@]}"
do
  echo "datahub delete --urn 'urn:li:dataset:(urn:li:dataPlatform:s3,trusted/lsp01/uc1/dataset/202210/$file,PROD)' --hard"
done
