import logging
from typing import List
import boto3
import os
import datahub.emitter.mce_builder as builder
from datahub.metadata.schema_classes import (
    DatasetSnapshotClass,
    TagAssociationClass
)
def get_client():
    s3_key = os.environ.get('MINIO_SERVER_ACCESS_KEY')
    s3_secret = os.environ.get('MINIO_SERVER_SECRET_KEY')
    s3_endpoint = 'http://'+os.environ.get('MINIO_ENDPOINT')+':'+os.environ.get('MINIO_PORT')
    s3client = boto3.client(
        's3',
        aws_access_key_id=s3_key,
        aws_secret_access_key=s3_secret,
        endpoint_url=s3_endpoint
    )
    return s3client

# This is added so that many files can reuse the function get_database()

def custom_tags(current: DatasetSnapshotClass) -> List[TagAssociationClass]:
    """ Returns tags to associate to a dataset depending on custom logic

    This function receives a DatasetSnapshotClass, performs custom logic and returns
    a list of TagAssociationClass-wrapped tags.

    Args:
        current (DatasetSnapshotClass): Single DatasetSnapshotClass object

    Returns:
        List of TagAssociationClass objects.
    """
    tag_strings = []
    s3client = get_client()
    paginator = s3client.get_paginator('list_objects_v2')
    page_iterator = paginator.paginate(Bucket='curated')
    for bucket in page_iterator:
        for file in bucket['Contents']:
            dataset = file['Key']
            if current.urn.endswith(dataset+",PROD)"):
                try:
                    metadata = s3client.head_object(Bucket='curated', Key=file['Key'])
                    for key in {'x-amz-meta-event_code',
                                'x-amz-meta-first_timestamp',
                                'x-amz-meta-panel_code',
                                'x-amz-meta-substation_code',
                                'x-amz-meta-trigger_timestamp'}:
                        if len(key.split('-')[-1]) > 0:
                            tag = key.split('-')[-1]+":"+metadata['ResponseMetadata']['HTTPHeaders'][key]
                            tag_strings.append(tag)
                except:
                    print("Failed {}".format(file['Key']))
    #print("urn=", current.urn)
    tag_strings = [builder.make_tag_urn(tag=n) for n in tag_strings]
    tags = [TagAssociationClass(tag=tag) for tag in tag_strings]
    if len(tag_strings) > 0:
        print("tag strings="+str(tag_strings))
    logging.info(f"Tagging dataset {current.urn} with {tag_strings}.")
    return tags

if __name__ == '__main__':
    get_client()