from setuptools import find_packages, setup

setup(
    name="s3-ingestion-metadata-to-tags",
    version="1.0",
    packages=find_packages(),
    install_requires=["acryl-datahub", "boto3"]
)