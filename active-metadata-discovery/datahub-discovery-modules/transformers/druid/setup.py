from setuptools import find_packages, setup

setup(
    name="druid-ingestion-spec-tag",
    version="1.0",
    packages=find_packages(),
    install_requires=["acryl-datahub", "pymongo"]
)