import logging
import os
import requests
import datetime
from typing import List
import json
import time
import sys
from pymongo import MongoClient

import datahub.emitter.mce_builder as builder
from datahub.metadata.schema_classes import (
    DatasetSnapshotClass,
    TagAssociationClass
)
def get_database():
    MONGODB_ENDPOINT="bd4nrg-ddp.vm.fedcloud.eu"
    MONGODB_PORT=27017
    MONGODB_USERNAME= "root"
    MONGODB_PASSWORD= "rootpassword"
    CONNECTION_STRING = "mongodb://"+MONGODB_USERNAME+":"+MONGODB_PASSWORD+"@"+MONGODB_ENDPOINT+":"+str(MONGODB_PORT)
    client = MongoClient(CONNECTION_STRING)
    return client['knowledgedb']

# This is added so that many files can reuse the function get_database()

def custom_tags(current: DatasetSnapshotClass) -> List[TagAssociationClass]:
    """ Returns tags to associate to a dataset depending on custom logic

    This function receives a DatasetSnapshotClass, performs custom logic and returns
    a list of TagAssociationClass-wrapped tags.

    Args:
        current (DatasetSnapshotClass): Single DatasetSnapshotClass object

    Returns:
        List of TagAssociationClass objects.
    """
    # DRUID_ENDPOINT = os.environ.get('DRUID_ENDPOINT')
    # DRUID_PORT = os.environ.get('DRUID_PORT')
    # DRUID_USERNAME = os.environ.get('DRUID_USERNAME')
    # DRUID_PASSWORD = os.environ.get('DRUID_PASSWORD')
    DRUID_ENDPOINT="bd4nrg-htap.vm.fedcloud.eu"
    DRUID_PORT=8888
    DRUID_USERNAME= "druid_system"
    DRUID_PASSWORD= "password2"
    druid_api_url = "http://{user}:{password}@{host}:{port}/druid/indexer/v1/".format(
        host=DRUID_ENDPOINT,
        port=DRUID_PORT,
        user=DRUID_USERNAME,
        password=DRUID_PASSWORD
    )
    knowledgedb = get_database()
    collection = knowledgedb["druidIngestionSpec"]
    tag_strings = []
    completeTask_url = druid_api_url+'completeTasks'
    r = requests.get( completeTask_url)
    tasks_descriptions = r.json()
    ingestion_specs_by_table = {}
    for task_description in tasks_descriptions:
        #print("task_description['status']=",task_description['status'], "current.urn=", current.urn)
        if task_description['status'] == 'SUCCESS' and current.urn.endswith(task_description['dataSource']+",PROD)"):
            element = datetime.datetime.strptime(task_description['createdTime'], '%Y-%m-%dT%H:%M:%S.%fZ')
            createdTime_epoch = datetime.datetime.timestamp(element)
            try:
                ingestion_specs_by_table[task_description["dataSource"]].append({'id':task_description['id'], 'createdTime':createdTime_epoch})
            except KeyError:
                ingestion_specs_by_table[task_description["dataSource"]] = []
            task_url = druid_api_url+'task/'+ task_description['id']
            r = requests.get(task_url)
            ingestion_spec = r.json()['payload']
            # print("name=", task_description['id'], " createdTime=", createdTime_epoch,
            #       # "payload=", str(ingestion_spec),
            #       "tableName=", task_description['dataSource'])
            #print(current.urn, "==", task_description['dataSource'])
            tag_strings.append("druidIngestionSpecID:"+task_description['id'])
            # dump to mongo
            entry = {'urn': current.urn, 'druidIngestionSpecID': task_description['id'], 'IngestionSpec': dict(ingestion_spec), 'createdTime:': createdTime_epoch}
            cursor = collection.find({"druidIngestionSpecID": task_description['id']})
            if len(list(cursor)) < 1:
                collection.insert_one(entry)
        # else:
        #     print("#####", current.urn, "!=", task_description['dataSource'])
    #print("urn=", current.urn)
    #TODO save latest tag only
    for dataSource, list_of_ingestions in ingestion_specs_by_table.items():
        if current.urn.endswith(task_description['dataSource']+",PROD)"):
            newest_ingestion = sorted(list_of_ingestions, key=lambda x: x["createdTime"], reverse=True)
            task_id = newest_ingestion[0]['id']
            # created_time = time.strftime("%Y-%m-%dT%H:%M:%SZ", time.localtime(newest_ingestion[0]['createdTime']))
            tag_strings.append("latestDruidIngestionSpecID:"+task_id)

    tag_strings = [builder.make_tag_urn(tag=n) for n in tag_strings]
    tags = [TagAssociationClass(tag=tag) for tag in tag_strings]
    if len(tag_strings) > 0:
        print("tag strings="+str(tag_strings))
        logging.info(f"Tagging dataset {current.urn} with {tag_strings}.")
    logging.info(f"No tags for dataset {current.urn} found.")
    return tags

if __name__ == '__main__':
    ds = DatasetSnapshotClass("a", [])
    custom_tags(ds)