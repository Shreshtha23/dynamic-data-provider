Run the extraction
```ssh
source ../.env
python app.py
```

Build docker image
```ssh
docker login registry.gitlab.com
docker build -t registry.gitlab.com/bd4nrg-technology-release/data-governance-and-management/dynamic-data-provider/active-metadata-discovery .
docker push registry.gitlab.com/bd4nrg-technology-release/data-governance-and-management/dynamic-data-provider/active-metadata-discovery:latest
```

Custom transformer

setup transformer with 
cd s3
pip install -e . 
# with pyton3

## dry run 
datahub ingest run -c LSP01-custom-transformer-test-cli.yaml -n

to set token for the client use
`DATAHUB_GMS_TOKEN=XXX`
 or load from .env
`export $(cat /home/ubuntu/dynamic-data-provider/active-metadata-discovery/datahub-discovery-modules/.env | xargs)`

